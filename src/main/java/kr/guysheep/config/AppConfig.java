package kr.guysheep.config;

import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 * @Since 2018. 03. 04.
 * @Version 1.0
 * @COPYRIGHT © GUYSHEEP ALL RIGHTS RESERVED.
 */
@EnableHystrix
@EnableHystrixDashboard
@EnableEurekaServer
@EnableZuulProxy
@Configuration
public class AppConfig {
}
